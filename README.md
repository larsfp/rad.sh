rad.sh - Radiation report
=========================

Fetch data from radnett.nrpa.no and display an overview for Norway

Sample output:
```

$ ./rad.sh

  Radiation 2014-12-05 14:37
  --------------------------
  Halden ☢       0.108 µSv/h
  Kjeller ☢      0.129 µSv/h

  Oslo           0.120 µSv/h
  Bergen         0.111 µSv/h
  Trondheim      0.093 µSv/h
  Bodø           0.103 µSv/h
  Vardø          0.075 µSv/h

```
